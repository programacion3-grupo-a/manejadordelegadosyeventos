﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejadorDelegadosYEventos
{

    public class BandejaDeEntrada : IObservable
    {
        public event NuevoMensajeDelegate MensajeRecibido;
        public event NuevoMensajeImportanteDelegate MensajeImportanteRecibido;

        private List<IObservador> observadores = new List<IObservador>();

        public void AgregarObservador(IObservador observador)
        {
            observadores.Add(observador);
        }

        public void RemoverObservador(IObservador observador)
        {
            observadores.Remove(observador);
        }

        public void RecibirMensaje(string remitente, string mensaje, bool esImportante)
        {
            Console.WriteLine($"Nuevo mensaje de {remitente}: {mensaje}");

            if (esImportante)
            {
                OnMensajeImportanteRecibido(remitente, mensaje);
            }
            else
            {
                OnMensajeRecibido(remitente, mensaje);
            }
        }

        protected virtual void OnMensajeRecibido(string remitente, string mensaje)
        {
            MensajeRecibido?.Invoke(remitente, mensaje);
        }

        protected virtual void OnMensajeImportanteRecibido(string remitente, string mensaje)
        {
            MensajeImportanteRecibido?.Invoke(remitente, mensaje);
        }
    }
}
