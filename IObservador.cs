﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejadorDelegadosYEventos
{
    public interface IObservador
    {
        void Actualizar(string remitente, string mensaje);
    }
}
