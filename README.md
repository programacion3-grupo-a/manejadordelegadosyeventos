# Manejador de Delegados y Eventos

Este proyecto en C# demuestra el uso de delegados, eventos y el patr�n de dise�o Observador. A continuaci�n, se presenta una descripci�n concisa del c�digo y su estructura.

## Estructura del Proyecto

### Program.cs
Punto de entrada principal del programa. Crea instancias de `BandejaDeEntrada` y observadores, y simula la llegada de mensajes.

### IObservable.cs
Interfaz `IObservable` que define m�todos para agregar, remover observadores y recibir mensajes.

### IObservador.cs
Interfaz `IObservador` que deben implementar los observadores, con el m�todo `Actualizar` para manejar actualizaciones.

### ObservadorMensajes.cs
Implementaci�n de `IObservador` para mensajes generales.

### ObservadorMensajesImportantes.cs
Implementaci�n de `IObservador` para mensajes importantes.

### Delegados
Estos delegados act�an como contratos que especifican c�mo deben ser los m�todos que se suscriben. Permiten la comunicaci�n entre componentes de la aplicaci�n.
```csharp
public delegate void NuevoMensajeDelegate(string remitente, string mensaje);
public delegate void NuevoMensajeImportanteDelegate(string remitente, string mensaje);
```

### BandejaDeEntrada.cs
Implementaci�n de `IObservable`. Contiene eventos que notifican a los observadores cuando se reciben mensajes y maneja la l�gica asociada.

## Uso

1. Instancia `BandejaDeEntrada`.
2. Crea observadores (`ObservadorMensajes` y `ObservadorMensajesImportantes`).
3. Agrega observadores mediante `AgregarObservador`.
4. Suscr�bete a eventos para cada observador.
5. Simula la llegada de mensajes con `RecibirMensaje`.
