﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejadorDelegadosYEventos
{
    public interface IObservable
    {
        void AgregarObservador(IObservador observador);

        void RemoverObservador(IObservador observador);

        void RecibirMensaje(string remitente, string mensaje, bool esImportante);
    }
}
