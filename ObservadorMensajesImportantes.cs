﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejadorDelegadosYEventos
{
    public class ObservadorMensajesImportantes : IObservador
    {
        public string Nombre { get; private set; }

        public ObservadorMensajesImportantes(string nombre)
        {
            Nombre = nombre;
        }

        public void Actualizar(string remitente, string mensaje)
        {
            Console.WriteLine($"{Nombre} recibe nuevo mensaje IMPORTANTE de {remitente}: {mensaje}");
        }
    }
}
