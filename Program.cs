﻿namespace ManejadorDelegadosYEventos
{
    public delegate void NuevoMensajeDelegate(string remitente, string mensaje);
    public delegate void NuevoMensajeImportanteDelegate(string remitente, string mensaje);

    class Program
    {
        static void Main()
        {
            BandejaDeEntrada bandejaDeEntrada = new BandejaDeEntrada();
            ObservadorMensajes observadorJuan = new ObservadorMensajes("Juan");
            ObservadorMensajes observadorMaria = new ObservadorMensajes("Maria");
            ObservadorMensajesImportantes observadorPedro = new ObservadorMensajesImportantes("Pedro");

            bandejaDeEntrada.AgregarObservador(observadorJuan);
            bandejaDeEntrada.AgregarObservador(observadorMaria);
            bandejaDeEntrada.AgregarObservador(observadorPedro);

            bandejaDeEntrada.MensajeRecibido += observadorJuan.Actualizar;
            bandejaDeEntrada.MensajeRecibido += observadorMaria.Actualizar;
            bandejaDeEntrada.MensajeImportanteRecibido += observadorPedro.Actualizar;

            bandejaDeEntrada.RecibirMensaje("Amigo1", "Hola, ¿cómo estás?", false);
            bandejaDeEntrada.RecibirMensaje("Amigo2", "Reunión a las 3 PM", true);
            bandejaDeEntrada.RecibirMensaje("Amigo3", "¿Vamos a cenar mañana?", false);

        }
    }
}
